﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class ProceduralGrid : MonoBehaviour
{
    public int xSize, ySize;

    Mesh mesh;
    Vector3[] vertices;

    void Awake()
    {
        Generate();
    }

    void Generate()
    {
        vertices = new Vector3[(xSize + 1) * (ySize + 1)];
        Vector2[] uv = new Vector2[vertices.Length];
        Vector4[] tangents = new Vector4[vertices.Length];
        Vector4 tangent = new Vector4(1, 0, 0, -1);
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Procedural Grid";
        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                vertices[i] = new Vector3(x, y);
                uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
                tangents[i] = tangent;
            }
        }
        mesh.vertices = vertices;
        int[] triangles = new int[6 * xSize * ySize];
        for (int vi = 0, ti = 0, y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; ti += 6, x++, vi++)
            {
                triangles[ti] = vi + 0;
                triangles[ti + 2] = triangles[ti + 3] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize + 2;
            }
        }
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
        mesh.tangents = tangents;
    }

    void OnDrawGizmos()
    {
        if (vertices == null) { return; }
        Gizmos.color = Color.black;
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(transform.TransformPoint(vertices[i]), .1f);
        }
    }
}